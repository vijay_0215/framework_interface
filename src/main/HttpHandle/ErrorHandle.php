<?php

namespace DreamCat\FrameInterface\HttpHandle;

use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * 捕获异常时的处理接口
 * @author vijay
 */
interface ErrorHandle
{
    /**
     * @param Throwable $throwable 捕获的异常
     * @return ResponseInterface 处理的响应对象
     */
    public function handle(Throwable $throwable): ResponseInterface;
}

# end of file
