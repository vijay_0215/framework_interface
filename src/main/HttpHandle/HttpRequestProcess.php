<?php

namespace DreamCat\FrameInterface\HttpHandle;

use Psr\Container\ContainerInterface;

/**
 * http请求处理主流程
 * @author vijay
 */
interface HttpRequestProcess
{
    /**
     * 处理请求
     * @param ContainerInterface $container 容器
     * @return void
     */
    public function handle(ContainerInterface $container): void;
}

# end of file
