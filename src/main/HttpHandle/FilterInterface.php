<?php

namespace DreamCat\FrameInterface\HttpHandle;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * http过滤器
 * @author vijay
 * @note 在进入路由器之前调用
 */
interface FilterInterface
{
    /**
     * 检查过滤请求，如果返回null则继续往下走，否则返回响应
     * @param ServerRequestInterface $request 服务器请求
     * @return ResponseInterface 响应消息
     */
    public function httpFilter(ServerRequestInterface $request): ?ResponseInterface;
}

# end of file
