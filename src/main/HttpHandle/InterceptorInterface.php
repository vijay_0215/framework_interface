<?php

namespace DreamCat\FrameInterface\HttpHandle;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * 拦截器接口
 * @author vijay
 * @note 调用路由器之后且找到handle之后调用
 */
interface InterceptorInterface
{
    /**
     * 拦截，如果返回null则继续往下走，否则返回响应
     * @param string $controllerClass 控制器类名
     * @param string $actName 方法名
     * @param ServerRequestInterface $request 服务器请求
     * @return ResponseInterface 响应消息
     */
    public function httpInterceptor(
        string $controllerClass,
        string $actName,
        ServerRequestInterface $request
    ): ?ResponseInterface;
}

# end of file
