<?php

namespace DreamCat\FrameInterface;

/**
 * 配置读取器接口
 * @author vijay
 */
interface ConfigReader
{
    /**
     * 读取配置
     * @param string $path 配置路径
     * @param mixed $defaultValue 不存在的情况下的默认值
     * @return mixed 配置内容
     */
    public function get(string $path, $defaultValue = null);

    /**
     * 获取项目根目录
     * @return string 项目根目录
     */
    public function getRootDir(): string;
}

# end of file
