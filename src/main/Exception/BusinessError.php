<?php
declare(strict_types=1);

namespace DreamCat\FrameInterface\Exception;

/**
 * 业务抛出的异常
 * @author vijay
 */
interface BusinessError extends \Throwable
{
}

# end of file
