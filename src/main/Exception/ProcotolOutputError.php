<?php
declare(strict_types=1);

namespace DreamCat\FrameInterface\Exception;

/**
 * 用于协议输出时的错误
 * @author vijay
 */
interface ProcotolOutputError extends \Throwable
{
}

# end of file
