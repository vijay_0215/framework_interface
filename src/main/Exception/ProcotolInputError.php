<?php
declare(strict_types=1);

namespace DreamCat\FrameInterface\Exception;

/**
 * 协议输入错误，用于收到的参数错误时抛出
 * @author vijay
 */
interface ProcotolInputError extends \Throwable
{
}

# end of file
