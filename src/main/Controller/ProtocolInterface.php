<?php

namespace DreamCat\FrameInterface\Controller;

use DreamCat\FrameInterface\Exception\BusinessError;
use DreamCat\FrameInterface\Exception\ProcotolInputError;
use DreamCat\FrameInterface\Exception\ProcotolOutputError;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * 协议接口
 * @author vijay
 * @note 输入转换调用时间在生成控制器方法参数前
 * @note 格式化调用时间在控制方法返回值不是标准响应接口时调用转换
 * @note 输出转换调用时间在控制器方法调用后拿到标准响应后
 */
interface ProtocolInterface
{
    /**
     * 输入转换
     * @param ServerRequestInterface $request 请求对象
     * @return ServerRequestInterface 转换后的请求对象
     * @throws ProcotolInputError
     */
    public function convertInput(ServerRequestInterface $request): ServerRequestInterface;

    /**
     * 输出转换
     * @param ResponseInterface $response 响应消息
     * @return ResponseInterface 响应消息
     * @throws ProcotolOutputError
     */
    public function convertOutput(ResponseInterface $response): ResponseInterface;

    /**
     * 格式化控制器方法的输出值
     * @param mixed $response 控制器方法返回值
     * @return ResponseInterface 标准响应消息
     * @throws ProcotolOutputError
     */
    public function formatOutput($response): ResponseInterface;

    /**
     * 处理异常的接口
     * @param BusinessError $businessError 控制器抛出的业务异常
     * @return ResponseInterface
     * @throws ProcotolOutputError
     */
    public function dealException(BusinessError $businessError): ResponseInterface;
}

# end of file
