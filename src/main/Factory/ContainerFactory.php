<?php

namespace DreamCat\FrameInterface\Factory;

use DreamCat\FrameInterface\ConfigReader;
use Psr\Container\ContainerInterface;

/**
 * 创建容器的工厂接口
 * @author vijay
 */
interface ContainerFactory
{
    /**
     * 根据配置创建容器
     * @param ConfigReader $configReader 配置读取器
     * @return ContainerInterface
     */
    public function create(ConfigReader $configReader): ContainerInterface;
}

# end of file
