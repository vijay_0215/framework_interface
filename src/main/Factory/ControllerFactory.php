<?php

namespace DreamCat\FrameInterface\Factory;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Http控制器工厂
 * @author vijay
 */
interface ControllerFactory
{
    /**
     * http请求处理入口
     * @param ContainerInterface $container 容器
     * @param ServerRequestInterface $request 请求对象
     * @return ResponseInterface 响应
     */
    public function httpHandle(ContainerInterface $container, ServerRequestInterface $request): ResponseInterface;
}

# end of file
