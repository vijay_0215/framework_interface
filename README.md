# framework_interface

## 介绍
框架的接口或抽象类相关，用于必要时导入，自行实现和扩展框架功能

## 接口列表
- [DreamCat\FrameInterface\ConfigReader](#configreader) 配置读取器接口
- [DreamCat\FrameInterface\Factory\ContainerFactory](#factorycontainerfactory) 容器创建工厂
- [DreamCat\FrameInterface\Factory\HttpRequestHandleFactory](#factoryhttprequesthandlefactory) Http请求处理器工厂
- [DreamCat\FrameInterface\HttpHandle\ErrorHandle](#httphandleerrorhandle) 捕获异常时的处理接口
- [DreamCat\FrameInterface\HttpHandle\HttpRequestProcess](#httphandlehttprequestprocess) http请求处理主流程

### ConfigReader
此接口主要用来做类型标注，不太需要重新实现，不过也为将来升级实现方式留个口子
```php
interface ConfigReader
{
    /**
     * 读取配置
     * @param string $path 配置路径
     * @param mixed $defaultValue 不存在的情况下的默认值
     * @return mixed 配置内容
     */
    public function get(string $path, $defaultValue = null);

    /**
     * 获取项目根目录
     * @return string 项目根目录
     */
    public function getRootDir(): string;
}
```

### Factory\ContainerFactory
```php
interface ContainerFactory
{
    /**
     * 根据配置创建容器
     * @param ConfigReader $configReader 配置读取器
     * @return ContainerInterface
     */
    public function create(ConfigReader $configReader): ContainerInterface;
}
```

### Factory\HttpRequestHandleFactory
```php
interface HttpRequestHandleFactory extends RequestHandlerInterface
{
}
```

### HttpHandle\ErrorHandle
```php
interface ErrorHandle
{
    /**
     * @param Throwable $throwable 捕获的异常
     * @return ResponseInterface 处理的响应对象
     */
    public function handle(Throwable $throwable): ResponseInterface;
}
```

### HttpHandle\HttpRequestProcess
```php
interface HttpRequestProcess
{
    /**
     * 处理请求
     * @return void
     */
    public function handle(): void;
}
```